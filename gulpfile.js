var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');


/* -------------------------------------------------------------
 *---- Dev Tasks ----*
 ------------------------------------------------------------- */


/* -------------------------------------------------------------
 *---- Start Browser Task ----*
 ------------------------------------------------------------- */

gulp.task('browserSync', function () {

    browserSync.init({
        server: {
            baseDir: 'app'
            /*proxy: 'local.dev'*/
        },
    })

})

/* -------------------------------------------------------------
 *---- Watch Tasks ----*
 ------------------------------------------------------------- */

gulp.task('sass', function () {

    return gulp.src('app/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({
            stream: true
        }))

});

gulp.task('watch', ['sass', 'browserSync'], function () {

    gulp.watch('app/scss/**/*.scss', ['sass'])
    gulp.watch('app/*.html', browserSync.reload);
    gulp.watch('app/js/*.js', browserSync.reload);

});

/* -------------------------------------------------------------
 *---- Optimization Tasks ----*
 ------------------------------------------------------------- */

gulp.task('useref', function () {
    return gulp.src('app/*.html')
        .pipe(useref())
        .pipe(gulpIf('*.js', uglify())) // only minify JavaScript files
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('dist'))
});

gulp.task('images', function () {
    return gulp.src('app/images/**/*.+(png|jpg|gif|svg)')
        .pipe(cache(imagemin({
            interlaced: true
        })))
        .pipe(gulp.dest('dist/images'))
});

/* -------------------------------------------------------------
 *---- Copying fonts ----*
 ------------------------------------------------------------- */

gulp.task('fonts', function () {
    return gulp.src('app/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'))
});

/* -------------------------------------------------------------
 *---- Cleaning ----*
 ------------------------------------------------------------- */

gulp.task('clean:dist', function () {
    return del.sync('dist');
});

/* -------------------------------------------------------------
 *---- Build Sequence ----*
 ------------------------------------------------------------- */

gulp.task('default', function (callback) {
    runSequence(['sass', 'browserSync', 'watch'], callback)
});

gulp.task('build', function (callback) {
    runSequence('clean:dist', ['sass', 'useref', 'images', 'fonts'], callback)
});
